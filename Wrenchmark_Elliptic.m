% -------------------------------------------------------------------------
% This code is made accessible to reproduce the numerical results
% presented in the article:
%    "Randomized residual-based error estimators for the Proper
%     Generalized Decomposition solution of parametrized problems"
%
% Authors: Olivier Zahm and Kathrin Smetana.
% Date:    October 2019.
% -------------------------------------------------------------------------
addpath(genpath('.'));
clear all
close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Creation of the wrench %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
wrench = Wrench(20);

% plot the geometry & the mesh
figure
subplot(2,1,1)
wrench.plotMesh();

subplot(2,1,2)
wrench.plotSol();
colorbar off
axis([-5 5 -2 2])
box on

%% Plot realizations of the Young Modulus
x_tir = wrench.x();
E = wrench.V*wrench.phi( x_tir );

clf
wrench.plot(E);
colorbar 
axis([-5 5 -2 2])
box on
set(gca,'YTickLabel',[],'XTickLabel',[],'ytick',[],'xtick',[]);

%%
wrench.plotSol(x_tir);

colorbar 
axis([-5 5 -2 2])
box on
set(gca,'YTickLabel',[],'XTickLabel',[],'ytick',[],'xtick',[]);

%% Define the metric (H1-norm)
Sigma = wrench.M + wrench.K0;
Sigma12 = chol(Sigma);

%% K-mean approx for E
NE = 50;
dimE = wrench.dimParam;
xi = quantizationGaussian1D_lloyds(NE);

%% Build the tensors

sA = cell(1+dimE,1);
sb = cell(1+dimE,1);

% 1/ Deterministic
sA{1} = wrench.K;
sb{1} = wrench.F;

% 3/ Young Modulus
for i=1:dimE
    sA{1+i} = cell(size(sA{1},1),1);
    sA{1+i}{1} = speye(NE);
    for j=1:size(wrench.K,1)
        sA{1+i}{j} = diag(sparse(exp(wrench.alpha(j,i)*xi)));
    end
    sb{1+i} = ones(NE,1);
end

% Finalize the tensor
A = CanonicalTensorFormat(sA);
b = CanonicalTensorFormat(cellfun(@(x){x},sb,'UniformOutput',0));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute the full PGD solution
minimizeResidual = false;
rankPrimal = 110;
u = CanonicalTensorFormat.GreedyRankOneALS(A,b,rankPrimal,minimizeResidual);

disp('Compute primal solution: done.')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute the exact error on a test set
Ntest = 1000;
indtest = randi(NE,Ntest,dimE);

error_exact = zeros(Ntest,1);
errorRelative_exact = zeros(Ntest,1);
uexact_test = zeros(u.sz(1),Ntest);
utest = double(evalAtIndices(u,indtest,[2:1+dimE]));
for i=1:Ntest
    if mod((i/Ntest)*20,1)==0; disp([ num2str(floor(i/Ntest*100)) '%']); end
    
    uexact_test(:,i) = wrench.evalSol(xi(indtest(i,:))',0);
    error_exact(i) = norm(Sigma12*(uexact_test(:,i) - utest(:,i)));
    errorRelative_exact(i) = error_exact(i)/norm(Sigma12*uexact_test(:,i));
end

disp('Exact relative error')
disp(mean(errorRelative_exact(:)))


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute (a few) dual randoms

K = [2 5 10];
nbDualTrial = 2;
rankDual = 10;

Z = cell(nbDualTrial,length(K));
Y = cell(nbDualTrial,length(K));
for i=1:nbDualTrial
    disp(floor(i/nbDualTrial*100))
    for j=1:length(K)
        Z{i,j} = CanonicalTensorFormat.ones(b.sz);
        Z{i,j}.spaces{1} = { Sigma12'*randn(size(Sigma12,1),K(j)) };
        Z{i,j} = Z{i,j}.updateProperties();
        Y{i,j} = CanonicalTensorFormat.GreedyRankOneALS(A',Z{i,j},rankDual,minimizeResidual);
    end
end

disp('Compute dual solutions: done.')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Monitor the error during the PGD algorithm
rankDual = 2;
maxiter = 50;

rankStagnation = rankDual;
maxiter = min(maxiter,u.rank-rankStagnation);
normB = norm((Sigma12')\wrench.F) * sqrt(NE^wrench.dimParam);

errorExact = zeros(maxiter,1);
ResidualNorm = zeros(maxiter,1);
Delta = zeros(maxiter,nbDualTrial,length(K));
DeltaExact = zeros(maxiter,nbDualTrial,length(K));
errorStagnation = zeros(maxiter,1);
for i=1:maxiter
    disp(i)
    
    utrunc = truncateTensor(u,1:i);
    res = A*utrunc-b;
    
    % Exact error
    utrunc_test = double(evalAtIndices(utrunc,indtest,[2:1+dimE]));
    errorExact(i) = rms(sqrt( sum((Sigma12*(uexact_test-utrunc_test) ).^2,1) ));
    errorExact(i) = errorExact(i) / rms(sqrt( sum((Sigma12*(uexact_test) ).^2,1) ));
    
    % Random dual
    timeCount=[];
    for j=1:nbDualTrial
        for indK=1:length(K)
            tic;
            Delta(i,j,indK) = evalDelta(Y{j,indK}.truncateTensor(1:rankDual),res);
            timeCount = [timeCount toc];
            
            Delta(i,j,indK) = Delta(i,j,indK) / evalDelta(Y{j,indK}.truncateTensor(1:rankDual),b);
            
            DeltaExact(i,j,indK) = rms( sqrt(sum((Z{j,indK}.spaces{1}{1}'*(uexact_test-utrunc_test)).^2,1)/K(indK)) );
            DeltaExact(i,j,indK) = DeltaExact(i,j,indK) / rms( sqrt(sum((Z{j,indK}.spaces{1}{1}'*(uexact_test)).^2,1)/K(indK)) );
        end
    end
    disp(['Time to compute Delta: ' num2str(mean(timeCount)) 's'])
    
    % Stagnation based error criteria
    correction = truncateTensor(u,(i+1):(i+rankStagnation) );
    correction.spaces{1} = cellfun(@(x) Sigma12*x,correction.spaces{1},'UniformOutput',0);
    errorStagnation(i) = norm(correction);
    
    correction = truncateTensor(u,1:(i+rankStagnation) );
    correction.spaces{1} = cellfun(@(x) Sigma12*x,correction.spaces{1},'UniformOutput',0);
    errorStagnation(i) = errorStagnation(i)/norm(correction); % Normalization
    
    
    % Dual norm of the residual
    tic;
    res.spaces{1} = cellfun(@(x)(Sigma12')\x,res.spaces{1},'UniformOutput',0);
    ResidualNorm(i) = norm(res)/normB;
    
    disp(['Time to compute the residual norm: ' num2str(mean(toc)) 's'])
end
clear res res_i utrunc

%%

indK = 1;

clf
semilogy(errorExact,'k')
hold on
semilogy(ResidualNorm,'--k')
semilogy(errorStagnation,'b')
semilogy(Delta(:,1,indK),'r')
semilogy(Delta(:,2:end,indK),'r','HandleVisibility','off')
semilogy(DeltaExact(:,:,indK),'r:')
hold off

legend({'Exact error','Residual norm',['Stagnation (rank=' num2str(rankStagnation) ')'],['Random dual (K=' num2str(K(indK)) ', rank=' num2str(rankDual) ')'],'Random dual (exact)'})
xlabel('PGD iteration for the primal solution')
ylabel('Error estimator')

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Delta = evalDelta(Y,res)

K = Y.sz(1)/res.sz(1);

tmp = sum(Y.*res,1);
tmp.spaces{1} = cellfun(@(x)x',tmp.spaces{1},'UniformOutput',0);
tmp = tmp.updateProperties();

Delta = norm(tmp) /sqrt(K);

end

