# RandomDual_PGD

This code is made accessible to reproduce the numerical results presented 
in the article:
   "Randomized residual-based error estimators for the Proper 
    Generalized Decomposition solution of parametrized problems"

Authors: Olivier Zahm and Kathrin Smetana.
Date:    October 2019.