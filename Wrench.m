classdef Wrench
    
    % Linear elasticity problem on a wrench parametrized by:
    %  - w: wave number (1-by-1 double)
    %  - x: coefficients (dimParam-by-1 double) of the KL decomposition of
    %       the Young modulus field which follows a logNormal distribution
    %       with zero mean and covariance Sigma
    % 
    % The problem is to solve for u=u(x,w) such that A(x,w)u=F where F is
    % the right-hand side and
    %    A(x,w) = K(x) - w*M
    % 
    % with M the mass matrix and K(x) is the stiffness matrix which writes 
    %    K(x) = sum_i  K{i}*phi_i(x)
    % 
    % ---------------------------------------------------------------------
    % This code is made accessible to reproduce the numerical results 
    % presented in the article:
    %    "Randomized residual-based error estimators for the Proper 
    %     Generalized Decomposition solution of parametrized problems"
    % 
    % Authors: Olivier Zahm and Kathrin Smetana.
    % Date:    October 2019.
    % ---------------------------------------------------------------------
    
    properties
        
        dimParam % Parametric dimension (= number of modes kept in the KL decomposition)
        MeshSize % diameter of the largest element
        PDEmodel % model for the PDE to solve
        Geometry % geometry of the wrench
        Sigma12  % Square root Sigma such that Sigma = Sigma12 * Sigma12'
        
        K  % Cell which contains the stiffness matrices
        K0 % Stiffness matrix associated with the Young Modulus E=1
        M  % Mass matrix
        F  % Right-hand side
        
        % Representation of the Young Modulus: E(x) = V*phi(x)
        phi   % Function handle: phi=@(x)exp(alpha*x)
        V     % Vector containing the Young Modulus modes
        alpha % Matrix resulting from the EIM approximation.
    end
    
    properties (Access = private)
        
        % Precomputations to speedup the code
        CenterOfElements
        TR
        results
        B
    end
    
    methods
        %------------------------------------------------------------------
        function [wrench,error] = Wrench(dim,hmax)
            
            % Definition of the model
            if nargin<1
                dim = 20;
            end
            if nargin<2
                hmax = 0.12;
            end
            
            wrench.dimParam = dim;
            wrench.MeshSize = hmax;
            
            % Initializations
            wrench = initGeo(wrench);
            wrench = initPDEmodel(wrench);
            wrench = initMesh(wrench);
            
            % parameter of the covariance function
            %  (x,y) ->  sigma * exp[ -( ||x-y||_2/l0 )^alpha ]
            alpha = 2;
            sigma = 0.4;
            l0 = 4;
            tol = 0.05;
            [wrench,error] = initYoungsModulusField(wrench,alpha,sigma,l0,tol);
            wrench = initKM(wrench);
            
        end
        %------------------------------------------------------------------
        function u = evalSol(wrench,varargin)
            
            A = evalA(wrench,varargin{:});
            u = A\wrench.F;
            
        end
        %------------------------------------------------------------------
        function A = evalA(wrench,x,w)
            
            if nargin<2
                x = wrench.x();
            end
            if nargin<3
                w = 0;
            end
            
            coef = wrench.phi(x);
            Kc = wrench.K{1}*coef(1);
            for i=2:length(coef)
                Kc = Kc + wrench.K{i}*coef(i);
            end
            A = Kc - w*wrench.M;
            
        end
        %------------------------------------------------------------------
        function x = x(wrench)
            % Draw the parameter (the log of the Young's Modulus)
            x = randn(wrench.dimParam,1);
        end
        %------------------------------------------------------------------
        function [VM] = VonMises(wrench,u,x,indElem)
            
            if nargin<3
                x = wrench.x();
                u = wrench.evalSol(x);
            end
            if nargin<4
                nbElements = size(wrench.CenterOfElements,2);
                indElem = 1:nbElements;
            end
            u = reshape(wrench.B*u,[size(wrench.B,1)/2 2]);
            if ~isempty(x)
                E = wrench.V*wrench.phi(x);
            else
                E = ones(size(wrench.V,1));
            end
            
            eta = 0.3;
            hookeEta = [ 1/(1-eta^2) 0 0 eta/(1-eta^2) ;...
                0 1/(1+eta)/2 1/(1+eta)/2 0 ; ...
                eta/(1-eta^2) 0 0 1/(1-eta^2)] ;
            
            [p,~,t] = meshToPet(wrench.PDEmodel.Mesh);
            [u1x,u1y] = pdegrad(p,t,u(:,1));
            [u2x,u2y] = pdegrad(p,t,u(:,2));
            
            VM = zeros(length(indElem),1);
            i=1;
            for k=indElem
                
                sigma = E(k) * hookeEta * [u1x(k) u1y(k) u2x(k) u2y(k)]';
                VM(i) = sqrt(sigma'*[ 1 1/2 0 ; 1/2 1 0 ; 0 0 3 ]*sigma);
                i=i+1;
            end
            
        end
        %------------------------------------------------------------------
        function plot(wrench,data)
            
            if length(data) == size(wrench.PDEmodel.Mesh.Nodes,2)
                % Nodal-based field
                pdemesh(wrench.PDEmodel,'XYData',data,'ColorMap','jet')
                axis equal
                
            elseif length(data) == size(wrench.PDEmodel.Mesh.Elements,2)
                % Element-based field : plot a piecewise constance field
                
                [p,~,t]= wrench.PDEmodel.Mesh.meshToPet();
                t(4,:)=[];
                
                x=p(1,:);
                y=p(2,:);
                P=[x(t(:));y(t(:))];
                T=reshape(1:size(P,2),[3 size(P,2)/3]);
                
                tmp=repmat(data(:)',3,1);
                h = trisurf(T',P(1,:),P(2,:),tmp(:));
                set(h,'edgecolor','none')
                
                grid off
                view(0,90)
                axis([-5 5 -3 2])
                axis equal
                
                colorbar
                caxis([min(data) max(data)])
            end
            
        end
        %------------------------------------------------------------------
        function plotGeometry(wrench)
            
            pdegplot(wrench.Geometry,'EdgeLabels','on','FaceLabels','off')
            
            axis equal
            
            set(gca,'YTickLabel',[]);
            set(gca,'XTickLabel',[]);
            set(gca,'ytick',[]);
            set(gca,'xtick',[]);
        end
        %------------------------------------------------------------------
        function plotMesh(wrench)
            
            pdemesh(wrench.PDEmodel,'ElementLabels','off')
            
            axis equal
            axis([-5 5 -2 2])
            
            set(gca,'YTickLabel',[]);
            set(gca,'XTickLabel',[]);
            set(gca,'ytick',[]);
            set(gca,'xtick',[]);
            
        end 
        %------------------------------------------------------------------
        function plotSol(wrench,u,VM)
            
            plotVM = 0;
            if nargin==1
                x = wrench.x();
                u = wrench.evalSol(x);
                VM = wrench.VonMises(u, x);
                plotVM = 1;
            end
            if nargin==2 && isa(u,'double')
                if numel(u)==wrench.dimParam
                    x = u;
                    u = wrench.evalSol(x);
                    VM = wrench.VonMises(u, x);
                    plotVM = 1;
                else
                    plotVM = 0;
                end
                
            end
            
            if nargin==3
                plotVM = 1;
            end
            
            u = reshape(wrench.B*u,[size(wrench.B,1)/2 2]);
            [p,e,t] = meshToPet(wrench.PDEmodel.Mesh);
            factor = 0.0005;
            pnew = p+u'*factor;
            
            if plotVM
                
                t(4,:)=[];
                
                x = pnew(1,:);
                y = pnew(2,:);
                P = [x(t(:));y(t(:))];
                T = reshape(1:size(P,2),[3 size(P,2)/3]);
                
                tmp = repmat(VM(:)',3,1);
                h = trisurf(T',P(1,:),P(2,:),tmp(:));
                set(h,'edgecolor','none')
                grid off
                
                view(0,90)
                colorbar
                caxis([min(VM) max(VM)])
                
                hold off
                
            else
                pdeplot(pnew,e,t)
                hold off
                
            end
            axis([-5 5 -3 2])
            axis equal
            
            set(gca,'YTickLabel',[],'XTickLabel',[],'ytick',[],'xtick',[]);
            
        end
        %------------------------------------------------------------------
    end% endMethods
    methods (Access = private) % Set the initialization functions...
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        function wrench = initGeo(wrench)
            
            l=3.5;
            e=0.4;
            r1=1.4;
            r2=1;
            c2= 0.85;
            r11=1.1;
            r22=0.6;
            
            r = (e^2+(l-1.4)^2-r1^2)/(2*r1-2*e);
            dd = l-r1/(r1+r)*(l-1.4);
            
            % Elementary geometries (Rectangles, Circles etc.)
            R1 = [3;4;-l*.8;-l*.8;l*.8;l*.8;-e;e;e;-e];
            C1 = [1;l;0;r1];
            C2 = [1;-l;0;r2];
            P1 = [2 6 0.45+l+r11*cos([0:5]/6*(2*pi) + pi/6 ) -0.26+r11*sin([0:5]/6*(2*pi) + pi/6 )]';
            P2 = [2 6 -l+r22*cos([0:5]/6*(2*pi) ) r22*sin([0:5]/6*(2*pi) )]';
            P0 = [3 4 1.4 1.4 dd dd  -1 1 1 -1]';
            P00= [2 4 -l/2 -l+0.5 -2.7 -l+0.5 0 c2 0 -c2]';
            E1 = [1 1.4 e+r r]';
            E2 = [1 1.4 -e-r r]';
            
            gd = [[R1;zeros(length(P1)-length(R1),1)] , ...
                [C1;zeros(length(P1)-length(C1),1)],...
                [C2;zeros(length(P1)-length(C2),1)],...
                P1,...
                P2,...
                [P0;zeros(length(P1)-length(P0),1)],...
                [P00;zeros(length(P1)-length(P00),1)],...
                [E1;zeros(length(P1)-length(E1),1)],...
                [E2;zeros(length(P1)-length(E2),1)]    ];
            
            % Assemble the geometry
            ns = char('R1','C1','C2','P1','P2','P0','P00','E1','E2');
            ns = ns';
            sf = '(R1+C1+C2+P0+P00)-P1-P2-E1-E2';
            [dl,bt] = decsg(gd,sf,ns);
            [dl,~] = csgdel(dl,bt);
            
            wrench.Geometry = dl;
            
        end
        %------------------------------------------------------------------
        function wrench = initPDEmodel(wrench)
            
            % Initialize
            wrench.PDEmodel = createpde(2);
            geometryFromEdges(wrench.PDEmodel,wrench.Geometry);
            
            % Set the boundary conditions
            applyBoundaryCondition(wrench.PDEmodel,'dirichlet','Edge',[10,11],'u',[0;0]);
            applyBoundaryCondition(wrench.PDEmodel,'neumann','Edge',[14],'g',[0;-1]);
            
        end
        %------------------------------------------------------------------
        function wrench = initMesh(wrench)
            
            generateMesh(wrench.PDEmodel,'Hmax',wrench.MeshSize,'GeometricOrder','linear');
            
            wrench.TR = triangulation(wrench.PDEmodel.Mesh.Elements',...
                wrench.PDEmodel.Mesh.Nodes');
            
            wrench.CenterOfElements = incenter(wrench.TR)';
            
        end
        %------------------------------------------------------------------
        function [wrench,error] = initYoungsModulusField(wrench,alpha,sigma,l0,tol0)
            
            % Distance matrix between the centers of the elements
            DXY = pdist(wrench.CenterOfElements','euclidean');
            DXY = squareform(DXY);
            
            % Creating the covariance matrix
            Sigma = sigma^2 *exp( -(DXY/l0).^alpha );
            
            % Truncated KL of Sigma
            [U,D] = eig(Sigma);
            D = diag(D);
            [~,ind] = sort(D,'descend');
            D = D(ind);
            U = U(:,ind);
            S12 = U*diag(sqrt(D));
            D = D(1:wrench.dimParam);
            U = U(:,1:wrench.dimParam);
            wrench.Sigma12 = U*diag(sqrt(D));
            
            % Do a EIM on the Young modulus
            Kmax = wrench.dimParam*3000;
            E = zeros(size(wrench.Sigma12,1),Kmax);
            tol = 0;
            for k=1:Kmax
                x = wrench.x();
                E(:,k) = exp(wrench.Sigma12*x);
                E0 = exp(S12*[x ; randn(size(S12,2)-size(x,1),1)]);
                tol = max( max(abs(E(:,k)-E0(:))), tol );
            end
            
            error = zeros(Kmax,1);
            res = E;
            Emax = max(abs(E(:)));
            ii = zeros(Kmax,1);
            jj = zeros(Kmax,1);
            rmax = Emax;
            k=0;
            while ( rmax > max(tol,tol0) && k < min(size(E)) ) || ( min(E(:)-res(:)) < 0)   
                k = k+1;
                [rmax,ind] = max(abs(res(:)));
                [ii(k),jj(k)] = ind2sub(size(res),ind);
                u = res(:,jj(k));
                v = res(ii(k),:)';
                res = res - (u*v')/res(ii(k),jj(k));
                error(k) = rmax;
            end
            error = error(1:k);
            ii = ii(1:k);
            jj = jj(1:k);
            
            wrench.phi = @(x) exp(wrench.Sigma12(ii,:)*x);
            wrench.alpha = wrench.Sigma12(ii,:);
            wrench.V = E(:,jj) / E(ii,jj);
            
            
            
        end
        %------------------------------------------------------------------
        function wrench = initKM(wrench)
            
            % M
            specifyCoefficients(wrench.PDEmodel,'m',0,'d',0,'c',0,'a',1,'f',[0;0]);
            FEM = assembleFEMatrices(wrench.PDEmodel , 'nullspace');
            wrench.M = FEM.Kc;
            wrench.F = FEM.Fc;
            wrench.B = FEM.B;
            
            % K0
            E = ones(size(wrench.Sigma12,1),1);
            % Hooke tensor for plain-stress problems, see https://www.mathworks.com/help/pde/ug/3-d-linear-elasticity-equations-in-toolbox-form.html?searchHighlight=linear%2520elasticity&s_tid=doc_srchtitle#bumr56f-1
            eta = 0.3; % Poissons ratio
            hookeEta = [ 1/(1-eta^2) 0 0 eta/(1-eta^2) ;...
                0 1/(1+eta)/2 1/(1+eta)/2 0 ; ...
                0 1/(1+eta)/2 1/(1+eta)/2 0 ; ...
                eta/(1-eta^2) 0 0 1/(1-eta^2)] ;
            hookeEta = reshape(hookeEta,[2,2,2,2]);
            hookeEta = permute(hookeEta,[1 3 2 4]);
            hookeEta = reshape(hookeEta,[4,4]);
            hookeTensor = @(Ex) repmat(hookeEta(:),1,length(Ex)) * spdiags(Ex(:),0,length(Ex),length(Ex)) ;
            c = @(region,state) hookeTensor( E(wrench.TR.pointLocation(region.x(:),region.y(:))) );
            
            specifyCoefficients(wrench.PDEmodel,'m',0,'d',0,'c',c,'a',0,'f',[0;0]);
            FEM = assembleFEMatrices(wrench.PDEmodel , 'nullspace');
            wrench.K0 = FEM.Kc;
            
            % K
            wrench.K = cell(size(wrench.V,2),1);
            for i=1:size(wrench.V,2)
                
                E = wrench.V(:,i);
                % Hooke tensor for plain-stress problems, see https://www.mathworks.com/help/pde/ug/3-d-linear-elasticity-equations-in-toolbox-form.html?searchHighlight=linear%2520elasticity&s_tid=doc_srchtitle#bumr56f-1
                c = @(region,state) hookeTensor( E(wrench.TR.pointLocation(region.x(:),region.y(:))) );
                
                % Specify all the coefficients
                specifyCoefficients(wrench.PDEmodel,'m',0,'d',0,'c',c,'a',0,'f',[0;0]);
                FEM = assembleFEMatrices(wrench.PDEmodel , 'nullspace');
                wrench.K{i} = FEM.Kc;
            end
            
        end
        %------------------------------------------------------------------
    end % endMethods
end % endClass





