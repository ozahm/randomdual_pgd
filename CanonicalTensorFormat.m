classdef CanonicalTensorFormat
    
    % Canonical tensor format:
    %    X = \sum_i^{rank} alpha_i X^{order}_i  ... X^{order}_i
    % 
    % where X^{order}_i are vectors or matrices.
    % 
    % ---------------------------------------------------------------------
    % This code is made accessible to reproduce the numerical results 
    % presented in the article:
    %    "Randomized residual-based error estimators for the Proper 
    %     Generalized Decomposition solution of parametrized problems"
    %
    % Authors: Olivier Zahm and Kathrin Smetana.
    % Date:    October 2019.
    % ---------------------------------------------------------------------
    
    properties
        
        rank    % rank of the tensor (integer)
        order   % order of the tensor (integer)
        data    % rank-by-1 double: data(i) = alpha_i
        spaces  % order-by-1 cell: contains the spaces{order}{i} = X^{order}_i 
        sz      % order-by-2 integer: dimension of the tensor: sz(order,i) = size(spaces{order},i)
        
    end
    
    
    methods
        %------------------------------------------------------------------
        function X = CanonicalTensorFormat(Spaces,Data)
            
            if nargin<2
                Data = ones(numel(Spaces{1}),1);
            end
            X.spaces = Spaces;
            X.data = Data;
            X = updateProperties(X);
            
        end
        %------------------------------------------------------------------
        function X = plus(X,Y)
            X.data = [X.data ; Y.data];
            X.spaces = cellfun(@(x,y)[x;y],X.spaces,Y.spaces,'UniformOutput',0);
            X = updateProperties(X);
        end
        %------------------------------------------------------------------
        function X = minus(X,Y)
            X = X + (-Y);
        end
        %------------------------------------------------------------------
        function X = uminus(X)
            X.data = -X.data;
        end
        %------------------------------------------------------------------
        function X = ctranspose(X)
            X.spaces = cellfun(@(x) cellfun(@(xx)xx',x,'UniformOutput',0) ,X.spaces,'UniformOutput',0);
            X = X.updateProperties();
        end
        %------------------------------------------------------------------
        function X = mtimes(X,Y)
            
            if isa(X,'CanonicalTensorFormat') && isa(Y,'CanonicalTensorFormat')
                dataXY = X.data*(Y.data');
                dataXY = dataXY(:);
                
                spacesXY = cell(X.order,1);
                for i=1:X.order
                    sX = repmat(X.spaces{i},1,Y.rank);
                    sY = repmat(Y.spaces{i}',X.rank,1);
                    spacesXY{i} = cellfun(@(xx,yy)xx*yy,sX,sY,'UniformOutput',0);
                    spacesXY{i} = spacesXY{i}(:);
                end
                
                X = CanonicalTensorFormat(spacesXY,dataXY);
                
            elseif isa(X,'CanonicalTensorFormat') && isa(Y,'double')
                X.data = Y*X.data;
            end
            
        end
        %------------------------------------------------------------------
        function X = times(X,Y)
            % Element-wise multiplication
            
            if isa(X,'CanonicalTensorFormat') && isa(Y,'CanonicalTensorFormat')
                
                dataXY = X.data*(Y.data');
                dataXY = dataXY(:);
                spaceX = cellfun(@(x) repmat(x,1,Y.rank) ,X.spaces,'UniformOutput',0);
                spaceY = cellfun(@(y) repmat(y',X.rank,1) ,Y.spaces,'UniformOutput',0);
                spacesXY = cellfun(@(x,y) cellfun(@(xx,yy)xx.*yy,x,y,'UniformOutput',0) ,spaceX,spaceY,'UniformOutput',0);
                spacesXY = cellfun(@(xy) xy(:),spacesXY,'UniformOutput',0);
                X = CanonicalTensorFormat(spacesXY,dataXY);
                
            elseif isa(X,'CanonicalTensorFormat') && isa(Y,'double')
                
                X.data = Y*X.data;
                
            end
            
        end
        %------------------------------------------------------------------
        function normX = norm(X,M)
            % Frobenius-type norm of the tensor:
            % normX = sum_{any indices} X_{...}^2
            
            if nargin<2
                M = CanonicalTensorFormat.speye(X.sz(:,1));
            end
            
            if all(X.sz(:,2)==1)
                % X is a vector.
                MX = M*X;
                
                Xs = cellfun(@(bs)[bs{:}],X.spaces,'UniformOutput',0);
                MXs = cellfun(@(bs)[bs{:}],MX.spaces,'UniformOutput',0);
                
                Xs = cellfun(@(x,y) x'*y ,Xs,MXs,'UniformOutput',0);
                Xs = cellfun(@(x) x(:) ,Xs,'UniformOutput',0);
                % Xs = prod([Xs{:}],2);
                tmp = ones(size(Xs{1}));
                for i=1:length(Xs)
                    tmp = tmp.*Xs{i};
                end
                Xs = tmp;
                Xs = reshape(Xs,X.rank,MX.rank);
                normX = X.data'*Xs*MX.data;
                normX = sqrt(abs(normX));
                
            else
                if nargin==1
                    normX = norm(vectorize(X));
                else
                    error('not implemented.')
                end
            end
            
        end
        %------------------------------------------------------------------
        function X = updateProperties(X)
            
            X.rank = numel(X.data);
            X.order = numel(X.spaces);
            X.sz = cellfun(@(x)size(x{1})',X.spaces,'UniformOutput',0);
            X.sz = [X.sz{:}]';
            
            X.data = X.data(:);
            X.spaces = X.spaces(:);
            X.spaces = cellfun(@(x)x(:),X.spaces,'UniformOutput',0);
            
        end
        %------------------------------------------------------------------
        function X = truncateTensor(X,ind)
            X.data = X.data(ind);
            X.spaces = cellfun(@(x)x(ind),X.spaces,'UniformOutput',0);
            X = X.updateProperties();
            
        end
        %------------------------------------------------------------------
        function X = evalAtIndices(X,ind,dims)
            % Evaluation of tensor entries
            %
            % X = evalAtIndices(X,ind)
            %  ind is an array of N-by-X.order
            %  X(k) <- X(I(k,1),I(k,2),...,I(k,d)), 1\le k \le N
            %  I is an array of N-by-x.order
            
            if all(X.sz(:,2)==1)
                % X is a vector.
                if nargin<2
                    dims = 1:X.order;
                end
                
                ind = mat2cell(ind,size(ind,1),ones(1,size(ind,2)));
                J = repmat({':'},1,X.order);
                J(dims) = ind;
                ind = J;
                
                % Evaluate the sub-tensor
                for i=1:X.order
                    if ~strcmp(':',ind{i})
                        X.spaces{i} = cellfun(@(x)x(ind{i},:),X.spaces{i},'UniformOutput',0);
                    end
                end
                X = X.updateProperties();
                
                % Evaluate the diagonal
                s = mat2cell([X.spaces{dims}],ones(X.rank,1),numel(dims));
                s = cellfun(@(x)prod([x{:}],2),s,'UniformOutput',0);
                nodims = setdiff(1:X.order,dims);
                X = CanonicalTensorFormat([X.spaces(nodims);{s}],X.data);
                
                % Expand X (or not)
                if isempty(nodims)
                    X = double(X);
                end
                
            else
                error('not implemented.')
            end
            
        end
        %------------------------------------------------------------------
        function X = vectorize(X)
            X.spaces = cellfun(@(bs) cellfun(@(x) x(:) ,bs,'UniformOutput',0) ,X.spaces,'UniformOutput',0);
            X = X.updateProperties();
        end
        %------------------------------------------------------------------
        function X = subTensor(X,varargin)
            
            for i=1:X.order
                if ~strcmp(':',varargin{k})
                    X.spaces{i} = X.spaces{i}(varargin{k},:);
                end
            end
        end
        %------------------------------------------------------------------
        function X = sum(X,ind)
            
            if nargin<2
                ind = 1:numel(X.sz);
            end
            
            for i=1:numel(ind)
                [Order,SubOrder] = ind2sub(size(X.sz),ind(i));
                X.spaces{Order} = cellfun(@(x)sum(x,SubOrder), X.spaces{Order},'UniformOutput',0);
            end
            X = X.updateProperties();
            
        end
        %------------------------------------------------------------------
        function x = double(X)
            
            if all(X.sz(:,2)==1) && X.order==2
                % The very easy case
                
                Xs = cellfun(@(bs)[bs{:}],X.spaces,'UniformOutput',0);
                x = Xs{1}*diag(sparse(X.data)) * Xs{2}';
                
            else
                xr = X.spaces{1}{1};
                for j=2:X.order
                    xr = kron(X.spaces{j}{1},xr);
                end
                x = X.data(1)*xr;
                
                for i=2:X.rank
                    xr = X.spaces{1}{i};
                    for j=2:X.order
                        xr = kron(X.spaces{j}{i},xr);
                    end
                    x = x + X.data(i)*xr;
                end
                
                tmp = X.sz(:)';
                tmp = tmp(1:find(tmp~=1,1,'last'));
                if ~all(tmp==1)
                    x = reshape(x,tmp);
                end
                
            end
            
        end
        %------------------------------------------------------------------
    end % endMethods
    %----------------------------------------------------------------------
    %----------------------------------------------------------------------
    %----------------------------------------------------------------------
    methods (Static)
        function X = randn(sz,r)
            
            if nargin<2
                r = 1;
            end
            Data = 1;
            Order = size(sz,1);
            Spaces = cell(Order,1);
            for i=1:Order
                Spaces{i} = cell(r,1);
                for j=1:r
                    Spaces{i}{j} = randn(sz(i,:));
                end
            end
            X = CanonicalTensorFormat(Spaces,Data);
            
        end
        %------------------------------------------------------------------
        function X = speye(sz)
            
            Order = size(sz,1);
            Spaces = cell(Order,1);
            for i=1:Order
                Spaces{i} = {speye(sz(i,:))};
            end
            X = CanonicalTensorFormat(Spaces);
            
        end
        %------------------------------------------------------------------
        function X = ones(sz)
            
            Order = size(sz,1);
            Spaces = cell(Order,1);
            for i=1:Order
                Spaces{i} = {ones(sz(i,:))};
            end
            X = CanonicalTensorFormat(Spaces);
            
        end
        %------------------------------------------------------------------
        function [w,err] = RankOneALS(A,b,w)
            
            maxIterations = 10;
            stagnation = 1e-2;
            
            % Preparation
            if nargin<3
                w = CanonicalTensorFormat.randn(b.sz);
                w.data = 1;
            end
            
            wAw = cellfun(@(x,y) cellfun(@(xx)trace(y{1}'*xx*y{1}),x), A.spaces,w.spaces,'UniformOutput',0);
            wAw = [wAw{:}]';
            
            ws = cellfun(@(bs) cellfun(@(x) x(:) ,bs,'UniformOutput',0) ,w.spaces,'UniformOutput',0);
            bs = cellfun(@(bs) cellfun(@(x) x(:) ,bs,'UniformOutput',0) ,b.spaces,'UniformOutput',0);
            bs = cellfun(@(bs)[bs{:}],bs,'UniformOutput',0);
            wb = cellfun(@(x,y) x'*y{1}, bs,ws,'UniformOutput',0);
            wb = [wb{:}]';
            
            k = 0;
            wprec = w;
            err = 2*stagnation;
            while k<maxIterations && err(end)>stagnation
                k = k+1;
                for i=1:w.order
                    
                    % Reduced system
                    wAw(i,:) = A.data;
                    Ai = A.spaces{i}{1}*prod(wAw(:,1));
                    for j=2:A.rank
                        Ai = Ai + A.spaces{i}{j}*prod(wAw(:,j));
                    end
                    wb(i,:) = b.data;
                    bi = bs{i}*(prod(wb)');
                    bi = reshape(bi,b.sz(i,:));
            
                    % Solve
                    wi = Ai\bi;
                    
                    % Updates
                    nwi = norm(wi,'fro');
                    wi = wi/nwi;
                    w.spaces{i} = {wi};
                    
                    wb(i,:) = (wi(:))'*bs{i};
                    wAw(i,:) = cellfun(@(x)trace(wi'*x*wi),A.spaces{i});
                end
                w.data = nwi;
                err(k) = sqrt(nwi^2+wprec.data^2 - 2*nwi*wprec.data*prod(cellfun(@(x,y)trace(x{1}'*y{1}),wprec.spaces,w.spaces)))/nwi;
                wprec = w;
            end
            
            
        end %endFunction
        %------------------------------------------------------------------
        %------------------------------------------------------------------
        function u = GreedyRankOneALS(A,b,rank,minimizeResidual)
            
            if nargin<3
                rank = 10;
            end
            if nargin<4
                minimizeResidual = false;
            end
            if minimizeResidual
                b = A'*b;
                A = A'*A;
            end
            
            % Greedy algorithm
            u = CanonicalTensorFormat.RankOneALS(A,b);
            res = b - A*u;
            
            % Let's go!
            for i=2:rank
                w = CanonicalTensorFormat.RankOneALS(A,res);
                res = res - A*w;
                u = u + w;
            end
            
        end %endFunction
    end % endMethodsStatics
end % endClass





