% -------------------------------------------------------------------------
% This code is made accessible to reproduce the numerical results
% presented in the article:
%    "Randomized residual-based error estimators for the Proper
%     Generalized Decomposition solution of parametrized problems"
%
% Authors: Olivier Zahm and Kathrin Smetana.
% Date:    October 2019.
% -------------------------------------------------------------------------
function [Delta,rankY] = intertwined(u,A,b,Y,Z,eta,maxiter,stag)

maxiter = min(maxiter,u.rank-stag);
rY = 1;
Ytrunc = Y.truncateTensor(1:rY);
eta = max(eta,1/eta);

Delta = zeros(maxiter,1);
rankY = ones(maxiter,1);
for i=1:maxiter
    disp(i)
    
    % Compare ||w|| and Delta=||Y'Aw||
    w = u.truncateTensor(max(1,i-stag):i);
    DeltaWexact = evalDelta(Z,w);
    DeltaW = evalDelta(Ytrunc,A*w);
    etaY = max(DeltaWexact/DeltaW,DeltaW/DeltaWexact);
    while etaY>eta && rY<Y.rank
        rY = rY+1;
        Ytrunc = Y.truncateTensor(1:rY);
        DeltaWexact = evalDelta(Z,w);
        DeltaW = evalDelta(Ytrunc,A*w);
        etaY = max(DeltaWexact/DeltaW,DeltaW/DeltaWexact);
    end
    
    utrunc = u.truncateTensor(1:i);
    Delta(i) = evalDelta(Ytrunc,A*utrunc-b) / evalDelta(Ytrunc,b);
    rankY(i) = rY;
    
end


end

function Delta = evalDelta(Y,res)

K = Y.sz(1)/res.sz(1);

tmp = sum(Y.*res,1);
tmp.spaces{1} = cellfun(@(x)x',tmp.spaces{1},'UniformOutput',0);
tmp = tmp.updateProperties();

Delta = norm(tmp) /sqrt(K);

end


