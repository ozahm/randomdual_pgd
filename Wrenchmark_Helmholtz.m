% -------------------------------------------------------------------------
% This code is made accessible to reproduce the numerical results presented 
% in the article:
%    "Randomized residual-based error estimators for the Proper
%     Generalized Decomposition solution of parametrized problems"
%
% Authors: Olivier Zahm and Kathrin Smetana.
% Date:    October 2019.
% -------------------------------------------------------------------------
addpath(genpath('.'));
clear all
close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Creation of the wrench %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

wrench = Wrench(1);

M = wrench.M;
K0 = wrench.K0;
F = wrench.F;

%% plot the geometry & the mesh

subplot(2,1,1)
wrench.plotMesh();

subplot(2,1,2)
uPlot = (K0-1*M)\F;
VonMises = wrench.VonMises(uPlot,[]);
wrench.plotSol( uPlot*9 ,VonMises );

colorbar off
axis([-5 5 -2 2])
box on

%% Define the metric (H1-norm)
Sigma = M + K0;
Sigma12 = chol(Sigma);

%% Range for w
Nw = 500;
w = linspace(0.5,1.2,Nw);
normSol = zeros(length(w),1);
for i=1:length(w)
    normSol(i) = norm( Sigma12*((K0-w(i)*M)\F) );
end

clf
semilogy(w,normSol,'k','linewidth',1.3)
xlabel('Parameter (wave number)')
legend('$\|u(\mu)\|_{\Sigma}$')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Creation of Au=b in a tensor format :
% 2D: Space x WaveNumber

%%% Build the tensors
sA = cell(2,1);
sb = cell(2,1);

% 1/ Deterministic
sA{1} = [{K0} ; {M}];
sb{1} = wrench.F;

% 2/ Wave number
sA{2} = cell(2,1);
sA{2}{1} = speye(Nw);
sA{2}{2} = -diag(sparse(w));
sb{2} = ones(Nw,1);

% Finalize the tensor
A = CanonicalTensorFormat(sA);
b = CanonicalTensorFormat(cellfun(@(x){x},sb,'UniformOutput',0));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute the full PGD solution
minimizeResidual = true;
u = CanonicalTensorFormat.GreedyRankOneALS(A,b,110,minimizeResidual);

% Compute the exact solution
uexact = zeros(u.sz(:,1)');
for i=1:Nw
    uexact(:,i) = (K0-w(i)*M)\F;
end

disp('Compute primal solution: done.')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute (a few) dual randoms

K = [1 2 6 20 60];
nbDualTrial = 10;
rankDual = 16;
bsz = b.sz;

Z = cell(nbDualTrial,length(K));
for j=1:length(K)
    for i=1:nbDualTrial
        Z{i,j} = CanonicalTensorFormat.ones(bsz);
        Z{i,j}.spaces{1} = { Sigma12'*randn(size(Sigma12,1),K(j)) };
        Z{i,j} = Z{i,j}.updateProperties();
    end
end

time = zeros(length(K),1);
Y = cell(nbDualTrial,length(K));
for j=1:length(K)
    tic
    disp(j)
    for i=1:nbDualTrial
        Y{i,j} = CanonicalTensorFormat.GreedyRankOneALS(A',Z{i,j},rankDual,minimizeResidual);
    end
    time(j) = toc/nbDualTrial;
end

disp([K;time'/time(1)])
disp('Compute dual solutions: done.')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compare the error estimators

rankPrimal = 10;
indK = 3;
rankStagnation = 4;

rankDual = rankStagnation;

% Exact error
utrunc = double(u.truncateTensor(1:rankPrimal));
errorExact = sqrt(sum(((Sigma12*(utrunc - uexact)).^2),1));
errorExact = errorExact ./ sqrt(sum(((Sigma12*(uexact)).^2),1)); % <- normalization
errorExact = errorExact(:);

% Stagnation
errorStagnation = u.truncateTensor( (rankPrimal+1):(rankPrimal+rankStagnation) );
errorStagnation = double(errorStagnation);
errorStagnation = sqrt(sum((Sigma12*errorStagnation).^2,1));
errorStagnation = errorStagnation ./ sqrt(sum(((Sigma12*(double(u.truncateTensor(1:(rankPrimal+rankStagnation))))).^2),1)); % <- normalization
errorStagnation = errorStagnation(:);

% Residual norm (dual norm of the residual)
res = double(A*u.truncateTensor(1:rankPrimal)-b);
residualNorm = sqrt(sum(((Sigma12')\res).^2,1)) ./ norm((Sigma12')\wrench.F); % <- normalization
residualNorm = residualNorm(:);

% Random duals (exact and approximated)
Delta = zeros(Nw,nbDualTrial);
DeltaExact = zeros(Nw,nbDualTrial);
for i=1:nbDualTrial
    DeltaExact(:,i) = sqrt(sum( (Z{i,indK}.spaces{1}{1}'*(utrunc-uexact)).^2 ,1) /K(indK));
    normalization = sqrt(sum( (Z{i,indK}.spaces{1}{1}'*(uexact)).^2 ,1) /K(indK));
    DeltaExact(:,i) = DeltaExact(:,i)./normalization(:); % <- normalization
    
    Yi = double(Y{i,indK}.truncateTensor(1:rankDual));
    normalization = sum( Yi.*repmat(double(b),[1,1,K(indK)]), 1);
    normalization = sqrt(sum(normalization.^2,3)/K(indK));
    Yi = sum( Yi.*repmat(res,[1,1,K(indK)]), 1);
    Yi = sqrt(sum(Yi.^2,3)/K(indK));
    Delta(:,i) = Yi(:)./normalization(:); % <- normalization
end

% Plots
clf
semilogy(w,errorExact,'k','linewidth',1.)
hold on
plot(w,residualNorm,'-.k','linewidth',1)
plot(w,DeltaExact(:,1),':r','linewidth',1.5)
plot(w,Delta(:,1),'r','linewidth',1.05)
plot(w,errorStagnation,'--b','linewidth',1.,'linewidth',1.05)
plot(w,0*w+1,':k','linewidth',1)

hold off

legend({'Exact error',...
        'Dual norm of the residual',...
        ['Exact random dual (K=' num2str(K(indK)) ')'],...
        ['Random dual (K=' num2str(K(indK)) ', rank=' num2str(rankDual) ')'],...
        ['Stagnation (rank=' num2str(rankStagnation) ')'
        ]},...
     'location','SouthWest')
xlabel('Parameter (wave number)')
ylabel('relative error')

axis([0.5,1.2,2e-4,2])


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The histograms of effectivity indices

rankPrimal = 10;
rankDual = [1 4 8 16];

indr = 1;
indK = 1;

% Exact error
utrunc = double(u.truncateTensor(1:rankPrimal));
errorExact = sqrt(sum(((Sigma12*(utrunc - uexact)).^2),1));
errorExact = errorExact ./ sqrt(sum(((Sigma12*(uexact)).^2),1)); % <- normalization
errorExact = errorExact(:);
res = double(A*u.truncateTensor(1:rankPrimal)-b);


nbar = 50;
etaLim = [1/30 30];

edges = @(x,n)logspace( log(min(x))/log(10),log(max(x))/log(10),n );
eta = edges(etaLim,500);

semilogx(1,-1,'.','Color',[1 1 1],'MarkerSize',0.001);% for the legend...
hold on
semilogx(1,-1,'.','Color',[1 1 1],'MarkerSize',0.001);% for the legend...

% Random duals 
etaDelta = zeros(Nw,nbDualTrial);
for i=1:nbDualTrial
    Yi = double(Y{i,indK}.truncateTensor(1:rankDual(indr)));
    normalization = sum( Yi.*repmat(double(b),[1,1,K(indK)]), 1);
    normalization = sqrt(sum(normalization.^2,3)/K(indK));
    Yi = sum( Yi.*repmat(res,[1,1,K(indK)]), 1);
    Yi = sqrt(sum(Yi.^2,3)/K(indK));
    Yi = Yi./normalization; % <- normalization
    etaDelta(:,i) = Yi(:)./errorExact(:);
end

hplot = histogram( etaDelta(:) ,edges(etaLim,nbar),'Normalization','probability','FaceColor','blue');
set(gca, 'XScale', 'log')
hold on


eta = edges(etaLim,1000);
theoreticPDF = fcdf(eta.^2,K(indK),K(indK));
theoreticPDF = [0 diff(theoreticPDF)];
factor = 1/sum(theoreticPDF)*(numel(eta)/nbar);
semilogx( eta, theoreticPDF*factor, 'k-','Linewidth',1.3)

legend({['$K=' num2str(K(indK)) '$'],['$rank(Y)=' num2str(rankDual(indr)) '$']})
hold off
set(gca,'YTickLabel',[]);
tmp = sort(hplot.Values);
axis([etaLim [0 max(theoreticPDF*factor)*1.4]])
set(gca,'ytick',[])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Re-compute (a few) dual randoms

K = 6;
nbDualTrial = 3;
rankDual = 16;

Z = cell(nbDualTrial,length(K));
Y = cell(nbDualTrial,length(K));
bsz = b.sz;
for i=1:nbDualTrial
    disp(i)
    for j=1:length(K)
        Z{i,j} = CanonicalTensorFormat.ones(bsz);
        Z{i,j}.spaces{1} = { Sigma12'*randn(size(Sigma12,1),K(j)) };
        Z{i,j} = Z{i,j}.updateProperties();
        Y{i,j} = CanonicalTensorFormat.GreedyRankOneALS(A',Z{i,j},rankDual,minimizeResidual);
    end
end

disp('Compute dual solutions: done.')

%% 
maxiter = 80;
rankStagnation = 5;

% Exact error and residual norm
errorExact = zeros(maxiter,1);
residualNorm = zeros(maxiter,1);
for i=1:maxiter
    disp(i)
    
    rankPrimal = i;
    
    % Exact error
    utrunc = double(u.truncateTensor(1:rankPrimal));
    tmp = sqrt(sum(((Sigma12*(utrunc - uexact)).^2),1));
    errorExact(i) = rms(tmp);
    errorExact(i) = errorExact(i) / rms(sqrt(sum((Sigma12*uexact).^2,1))); % <- normalization
    
    % Residual norm (dual norm of the residual)
    tmp = A*u.truncateTensor(1:rankPrimal)-b;
    tmp.spaces{1} = cellfun(@(x)(Sigma12')\x,tmp.spaces{1},'UniformOutput',0);
    residualNorm(i) = norm(tmp);
    
    tmp = b;
    tmp.spaces{1} = cellfun(@(x)(Sigma12')\x,tmp.spaces{1},'UniformOutput',0);
    residualNorm(i) = residualNorm(i) / norm(tmp); % <- normalization
    
end

% Stagnation
errorStagnation = zeros(maxiter,1);
for i=1:maxiter
    disp(i)
    rankPrimal = i;
    
    tmp = u.truncateTensor( (rankPrimal+1):(rankPrimal+rankStagnation) );
    tmp.spaces{1} = cellfun(@(x)Sigma12*x,tmp.spaces{1},'UniformOutput',0);
    errorStagnation(i) = norm(tmp);
    
    tmp = u.truncateTensor( 1:(rankPrimal+rankStagnation) );
    tmp.spaces{1} = cellfun(@(x)Sigma12*x,tmp.spaces{1},'UniformOutput',0);
    errorStagnation(i) = errorStagnation(i) / norm(tmp); % <- normalization
    
end

%% The intertwined algorithm

stag = 5;
eta = 2;

rankY = cell(nbDualTrial,length(K));
Delta = cell(nbDualTrial,length(K));
for indK=1:length(K)
    for j=1:nbDualTrial
        disp(j)
        [Delta{j,indK},rankY{j,indK}] = intertwined(u,A,b,Y{j,indK},Z{j,indK},eta,maxiter,stag);
    end
end

%%



indK = 1;
clf
nbblocks = 5;
meanRankY = mean([rankY{:,indK}],2);
[ind,rYu] = kmeans(meanRankY,nbblocks);
[rYu,perm] = sort(rYu);
indices = zeros(max(ind),1);
for i=1:max(ind)
    indices(i) = find(ind==perm(i),1,'last');
end


% meanRankY = mean([rankY{:,indK}],2)';
s=pcolor(indices,[1e-7 1e1],[rYu';rYu']);
s.HandleVisibility='off';
s.FaceAlpha=1;
hold on
set(gca,'YScale', 'log')
colormap(linspace(1,0.8,nbblocks)'*[1 1 1])
h = colorbar;
h.Label.String = '$\text{rank}(Y)$';
% h.Limits = [0,35];

semilogy(errorExact,'k','LineWidth',2)
hold on
semilogy(residualNorm,'--k')
semilogy(errorStagnation,'b')
semilogy([Delta{:,indK}],'r')


hold off
legend({'Exact error','Residual norm',['Stagnation (rank=' num2str(rankStagnation) ')'],['Random dual (K=' num2str(K(indK)) ')']},...
    'location','NorthEast')
xlabel('PGD iteration for the primal solution')
ylabel('Relative errors')

axis([0,maxiter,1e-5 2])
