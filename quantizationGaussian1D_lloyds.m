% -------------------------------------------------------------------------
% This code is made accessible to reproduce the numerical results
% presented in the article:
%    "Randomized residual-based error estimators for the Proper
%     Generalized Decomposition solution of parametrized problems"
%
% Authors: Olivier Zahm and Kathrin Smetana.
% Date:    October 2019.
% -------------------------------------------------------------------------
function [codebook,w] = quantizationGaussian1D_lloyds(N)

kmax = 100;
codebook = linspace(0,1,N+1);
codebook = centerOfIntervals(codebook);
codebook = norminv(codebook);
partition = [-inf centerOfIntervals(codebook) inf];

k = 0;
err = 1;
while err > 1e-4 && k<kmax
    k = k+1;
    codebook_new = zeros(1,N);
    w = zeros(N,1);
    for i=1:N
        mu_i = normcdf(partition(i+1)) - normcdf(partition(i));
        esperance_i = -( exp(-partition(i+1)^2/2) - exp(-partition(i)^2/2) )/sqrt(2*pi);
        codebook_new(i) = esperance_i/mu_i;
        w(i)=mu_i;
    end
    err = norm(codebook - codebook_new)/norm(codebook);
    codebook = codebook_new;
    partition = [-inf centerOfIntervals(codebook) inf];
    
end

function x = centerOfIntervals(x)

x = sum([x,0;0,x])/2;
x = x(2:end-1);